"use strict";
$(function () {

	// chart
	var ctx = $("#year");
	var lineChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: ["Jun 2015", "Jan 2016", "June 2016", "Jan 2017", "June 2017", "Jan 2018"],
			datasets: [
				{
					data: [0,12500,2000,15000,4000,20000],
					backgroundColor: "rgba(139, 95, 255, .6)",
					borderColor: "transparent",
					pointRadius: 0,
					spanGaps: true,
				},
				{
					data: [0,17000,5000,22000,8000,40000],
					backgroundColor: "rgba(53, 251, 254, 0.6)",
					borderColor: "transparent",
					pointRadius: 0,
					spanGaps: true,
				}
			]
		},
		options: {
	        legend: {
	            display: true,
	            position: 'top',
	            reverse: true,
	            labels: {
	                fontColor: '#7d7d7d'
	            }
	    	}
		}
	});

});